﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHeroesApp.Models
{
    public class Genre : Customer
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }

    }
}
