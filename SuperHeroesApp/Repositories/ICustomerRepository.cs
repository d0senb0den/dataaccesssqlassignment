﻿using SuperHeroesApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHeroesApp.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetCustomer(int id);
        public Customer GetCustomerByName(string name);
        public List<Genre> GetCustomerAndPopularGenre(int id);
        public List<Customer> GetAllCustomers();
        public List<Customer> GetAllCustomersPagination(int offset, int limit);

        public List<Invoice> GetAllCustomersByInvoice();
        public void AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public Dictionary<string, int> CustomersInEachCountry(Customer customer);

    }
}
