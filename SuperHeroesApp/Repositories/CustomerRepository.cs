﻿using Microsoft.Data.SqlClient;
using SuperHeroesApp.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperHeroesApp.Repositories
{
    public class CustomerRepository : ICustomerRepository

    {
        /// <summary>
        /// Gets all customers from Database
        /// </summary>
        /// <returns>List<Customer></returns>
        /// <exception cref="SqlException">When something goes wrong</exception>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new();

                                temp.CustomerID = (int)reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) temp.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) temp.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }

        /// <summary>
        /// Gets a specific amount of customers based on Offset and Limit
        /// </summary>
        /// <param name="offset">Starting point of list</param>
        /// <param name="limit">Amount of customers</param>
        /// <returns>List<Customer></returns>
        /// <exception cref="SqlException">When something goes wrong</exception>
        public List<Customer> GetAllCustomersPagination(int offset, int limit)
        {
            List<Customer> customerList = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@offset", offset);
                        command.Parameters.AddWithValue("@limit", limit);
                        command.ExecuteNonQuery();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new();

                                temp.CustomerID = (int)reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) temp.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) temp.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }

        /// <summary>
        /// Gets all the customers in descending order based on their Total from Invoice
        /// </summary>
        /// <returns>List<Invoice></Invoice></returns>
        /// <exception cref="SqlException">Catches when something goes wrong</exception>
        public List<Invoice> GetAllCustomersByInvoice()
        {
            List<Invoice> customerList = new();
            string sql = "SELECT DISTINCT FirstName, Total, Customer.CustomerId FROM Customer JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId ORDER BY Total DESC";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Invoice temp = new();

                                temp.FirstName = reader.GetString(0);
                                temp.Total = (double)reader.GetDecimal(1);
                                if (!reader.IsDBNull(2)) temp.CustomerID = (int)reader.GetInt32(2);
                                customerList.Add(temp);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }

        /// <summary>
        /// Gets a specific customer based on Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Customer</returns>
        /// <exception cref="SqlException">Catches when something goes wrong</exception>
        public Customer GetCustomer(int id)
        {
            string sql = @"Select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = (@id)";
            Customer temp = new();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("id", id);
                        command.ExecuteNonQuery();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                temp.CustomerID = (int)reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) temp.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) temp.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return temp;
        }

        /// <summary>
        /// Gets a customer based on Name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Customer</returns>
        /// <exception cref="SqlException">Catches when something goes wrong</exception>
        public Customer GetCustomerByName(string name)
        {
            string sql = @"Select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE (@name)";
            Customer temp = new();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("name", name);
                        command.ExecuteNonQuery();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                temp.CustomerID = (int)reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) temp.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) temp.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return temp;
        }

        /// <summary>
        /// Gets a specific customer based on Id and displays their most prominent Genre based on Invoice
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<Genre></Genre></returns>
        /// <exception cref="SqlException">Catches when something goes wrong</exception>
        public List<Genre> GetCustomerAndPopularGenre(int id)
        {
            //string sql = @"Select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = (@id)";
            string sql = @"SELECT FirstName, LastName, Genre.Name, Customer.CustomerId, COUNT(Genre.Name) AS PopularGenre 
FROM Customer INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId 
INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId 
INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId 
INNER JOIN Genre ON Track.GenreId = Genre.GenreId 
GROUP BY FirstName, LastName, Customer.CustomerId, Genre.Name 
HAVING COUNT(Genre.Name) >= 1 AND Customer.CustomerId = (@id) 
ORDER BY COUNT(Genre.Name) DESC";

            List<Genre> customer = new();
            List<Genre> temporary = new();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("id", id);
                        command.ExecuteNonQuery();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Genre customerGenre = new();
                                customerGenre.FirstName = reader.GetString(0);
                                customerGenre.LastName = reader.GetString(1);
                                customerGenre.Name = reader.GetString(2);
                                customerGenre.CustomerID = (int)reader.GetInt32(3);
                                customerGenre.Count = (int)reader.GetInt32(4);
                                temporary.Add(customerGenre);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            int checkValue = temporary[0].Count;
            foreach (var item in temporary)
            {
                if (item.Count >= checkValue)
                {
                    customer.Add(item);
                }
            }
            return customer;
        }
        /// <summary>
        /// Adds a new customer
        /// </summary>
        /// <param name="customer"></param>
        /// <exception cref="SqlException">Catches when something goes wrong</exception>
        public void AddNewCustomer(Customer customer)
        {
            string sql = "INSERT INTO Customer(FirstName, LastName, Email, PostalCode, Phone) VALUES(@FirstName, @LastName, @Email, @PostalCode, @Phone)";
            Customer temp = new();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone);
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// Updates a customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>bool</returns>
        /// <exception cref="SqlException">Catches when something goes wrong</exception>
        public bool UpdateCustomer(Customer customer)
        {
            bool result = false;
            string sql = @"UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email WHERE CustomerId = @CustomerId";
            Customer temp = new();
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CustomerId", customer.CustomerID);
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        result = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Prints out countries and amount of customers in each country
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>Dictionary<string, int></returns>
        /// <exception cref="SqlException">Catches when something goes wrong</exception>
        public Dictionary<string, int> CustomersInEachCountry(Customer customer)
        {
            Country country = new();
            Dictionary<string, int> dict = new Dictionary<string, int>();
            string sql = "SELECT Country, COUNT(Country) FROM Customer GROUP BY Country ORDER BY COUNT(Country) DESC";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                if (!reader.IsDBNull(0)) country.CountryName = reader.GetString(0);
                                if (!reader.IsDBNull(1)) country.Count = reader.GetInt32(1);
                                if (dict.ContainsKey(country.CountryName))
                                {
                                    continue;
                                }
                                else
                                {
                                    dict.Add(country.CountryName, country.Count);
                                }

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            foreach (var item in dict)
            {
                Console.WriteLine($"---- {item.Key} {item.Value} ------ ");
            }
            return dict;
        }

    }
}
