﻿

using SuperHeroesApp.Models;
using SuperHeroesApp.Repositories;

ICustomerRepository repository = new CustomerRepository();
Customer customer = new();
//<------------------------------------------>
//Uncomment which query you wish to test below

//TestSelect(repository);
//TestSelectAll(repository);
//TestSelectByName(repository);
//TestSelectAllPagination(repository);
//TestAddCustomer(repository);
//TestUpdate(repository, customer);
//TestSelectCountryAndCount(repository, customer);
//TestPrintCustomersByInvoice(repository, customer);
//TestPrintCustomersByGenres(repository, 1);
//<------------------------------------------->

static void TestSelectAll(ICustomerRepository repository)
{
    PrintCustomers(repository.GetAllCustomers());
}
static void TestSelectAllPagination(ICustomerRepository repository)
{
    PrintCustomers(repository.GetAllCustomersPagination(20, 20));
}

static void TestSelect(ICustomerRepository repository)
{
    PrintCustomer(repository.GetCustomer(2));
}
static void TestSelectByName(ICustomerRepository repository)
{
    PrintCustomer(repository.GetCustomerByName("Pontus"));
}
static void TestSelectCountryAndCount(ICustomerRepository repository, Customer customer)
{
    repository.CustomersInEachCountry(customer);
}

static void TestAddCustomer(ICustomerRepository repository)
{
    Customer test = new()
    {
        FirstName = "Pontus",
        LastName = "Gillson",
        Country = "Sverige",
        PostalCode = "126 35 ",
        Phone = "073 666 42069",
        Email = "p.chillson@gmail.nu",
    };
    repository.AddNewCustomer(test);
}

static void TestUpdate(ICustomerRepository repository, Customer customer)
{
    customer.CustomerID = 7;
    customer.FirstName = "Astroid";
    customer.LastName = "Gröber";
    customer.Country = "Mars";
    customer.PostalCode = "133 37";
    customer.Phone = "+69 420 96";
    customer.Email = "astroid.gröber@comet.crater";
    repository.UpdateCustomer(customer);
}

static void TestPrintCustomersByInvoice(ICustomerRepository repository, Customer customer)
{
    PrintCustomersInvoices(repository.GetAllCustomersByInvoice());
}

static void TestPrintCustomersByGenres(ICustomerRepository repository, int id)
{
   PrintCustomersGenres(repository.GetCustomerAndPopularGenre(id));
}

static void PrintCustomers(IEnumerable<Customer> customers)
{
    foreach (Customer customer in customers)
    {
        PrintCustomer(customer);
    }

}
static void PrintCustomer(Customer customer)
{
    Console.WriteLine($"---- {customer.CustomerID} {customer.FirstName} {customer.LastName} {customer.Address} {customer.State} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Fax} {customer.Email} {customer.Company} {customer.City} ---");
}

static void PrintCustomersGenres(IEnumerable<Genre> customers)
{
    foreach (Genre customer in customers)
    {

        Console.WriteLine($" Customer ID: {customer.CustomerID} Firstname: {customer.FirstName} Surname: {customer.LastName} Most popular Genre: {customer.Name} Amount: {customer.Count}");
    }
}

static void PrintCustomersInvoices(IEnumerable<Invoice> customers)
{
    foreach (Invoice customer in customers)
    {

        Console.WriteLine($" Customer ID: {customer.CustomerID} Firstname: {customer.FirstName} Surname: {customer.LastName} Invoice Total: {customer.Total}");
    }
}






